import {GameScreen} from "./screens/game";

export const Game = () => {

    const create = displayList => GameScreen(displayList);
    const start = () => document.getElementById("titleCard").style.display = "none";

    return {create, start};
};
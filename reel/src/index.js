import {Bootstrap} from "../../pixi-bootstrap/build/index.js"; //Should be npm package - don't have paid npm account!
import {Game} from "./game";
import Config from "./config.json";

const bootstrap = Bootstrap({height:Config.size.height, width:Config.size.width, backgroundColor:0xFFFFFF});
const game = Game();

bootstrap.setup().then(game.create).then(game.start);
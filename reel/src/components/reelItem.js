export const ReelItem  = (display, data) => {

    return {
        getDisplay(){
            return display;
        },
        move(dist){
            let endReached = false;
            display.y += dist;
            if(display.y > Math.abs(data.startY + data.reelSize)) {
                const overflow = display.y - Math.abs(data.startY + data.reelSize);
                display.y = data.startY + overflow;
                endReached = true;
            }
            return endReached;
        },
        getFrame(){
            return display.currentFrame;
        },
        setFrame(frame){
            display.gotoAndStop(frame);
        },
        getPosition(){
            return display.y;
        }
    }
};
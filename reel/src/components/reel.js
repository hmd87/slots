import gsap from "gsap/";
import {randomNumber, sanitise} from "../utils/helpers";
import {ReelItem} from "./reelItem";

const createSymbol = (create, pos, frame, data) => {
    const symbol = create("ui.png", "symbol");
    symbol.y = pos;
    symbol.gotoAndStop(frame);
    return ReelItem(symbol, data);
};

export const Reel = (display, root, data, id) => {

    const symbols = [];
    let result = [];
    let s;

    let i = 0;
    data.layout.forEach(l => {
        symbols.push(createSymbol(root.graphics.createAnimatedSprite, data.startY + (data.reelSize * i), l === "N" ? randomNumber(0, 5) : 6, data));
        i++;
        if(l === "S"){
            symbols.push(createSymbol(root.graphics.createAnimatedSprite, data.startY + (data.reelSize * i ), 7, data));
            symbols.push(createSymbol(root.graphics.createAnimatedSprite, data.startY + (data.reelSize * (i + 1)), 8, data));
            i += 2;
        }
    });

    symbols.forEach(s => display.addChild(s.getDisplay()));

    let speed = 0;
    let modifier = 1;
    let stopTime = 1.5;
    let stopEase = "circ.out";

    const increaseSpeedTo = (_speed, duration = 1) => {
        if(s) gsap.killTweensOf(s);
        s = {speed};
        gsap.to(s, {
            duration, speed: _speed, onUpdate: () => {
                speed = s.speed;
            }
        });
    };

    const resolveStacked = (frame, symbolIndex) => {
        const index = [6,7,8].indexOf(frame);
        let toChange;
        if(index === -1) return;
        if(index === 0) toChange = [symbolIndex, symbolIndex + 1, symbolIndex + 2];
        if(index === 1) toChange = [symbolIndex, symbolIndex + 1, symbolIndex - 1];
        if(index === 2) toChange = [symbolIndex, symbolIndex - 1, symbolIndex - 2];
        sanitise(toChange, symbols.length).forEach(i => symbols[i].setFrame(randomNumber(0, 5)));
    };

    const move = () => {
        if(!speed) return;
        symbols.forEach((s, i) => {
            if(s.move(speed * modifier)){
                if(result[0]) {
                    resolveStacked(s.getFrame(), i);
                    s.setFrame(result.shift());
                    if (!result.length) {
                        moveToFinal(s.getPosition());
                    }
                }
            }
        })
    };
    gsap.ticker.add(move);

    const  moveToFinal = (y) => {
        const pos = {y, o:y};
        gsap.to(pos, {
            duration:stopTime,
            y:0,
            ease:stopEase,
            onUpdate:() => {
                increaseSpeedTo( Math.abs(pos.o - pos.y), 0);
                pos.o = pos.y;
            },
            onComplete:() => {
                console.log(`REEL-${id}: STOPPED`, Date.now());
                speed = 0;
            }
        });
    };

    return {
        setModifier(_mod){
            modifier = _mod;
        },
        setSpeed(_speed){
            speed = _speed;
        },
        increaseSpeedTo,
        randomStop(_stopTime, _stopEase){
            stopTime = _stopTime;
            stopEase = _stopEase;
            result = Math.round(Math.random()) ? [randomNumber(0, 5), randomNumber(0, 5), randomNumber(0, 5)] : [8,7,6];
        }
    }
};
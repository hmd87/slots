


export const StandardAnimator  = display => {

    const rollover = () => Promise.resolve();
    const rollout = () => Promise.resolve();
    const click = () => Promise.resolve();
    const activate = () => Promise.resolve();
    const deactivate = () => Promise.resolve();

    return {rollover, rollout, click, activate, deactivate}

}

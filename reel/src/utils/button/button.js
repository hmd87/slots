import {MouseInteraction} from './MouseInteraction.js'
import ButtonEvents from './ButtonEvents.js'
import {EventHandler} from './EventHandler'

export const Button = ({clickLayer, oneShot = true}) => {

    const eventHandler = EventHandler(ButtonEvents);
    const mouseInteraction = MouseInteraction(clickLayer);

    let active = false;
    let over = false;
    let down = false;
    const disableInteraction = () => mouseInteraction.deactivate();

    const activate = () => {
        if (active) return void 0;
        active = true;
        mouseInteraction.activate();
        eventHandler.handleEvent(ButtonEvents.ACTIVATE);
    };

    const deactivate = () => {
        if (!active) return void 0;
        active = false;
        mouseInteraction.deactivate();
        eventHandler.handleEvent(ButtonEvents.DEACTIVATE);
    };


    const click = event => {
        if ((event.data ? event.data.originalEvent.target : event.target).nodeName.toUpperCase() === "CANVAS") {
            eventHandler.handleEvent(ButtonEvents.CLICK);
            if (oneShot) deactivate();
        }
    };

    const simulateClick = () => {
        eventHandler.handleEvent(ButtonEvents.CLICK);
        if (oneShot) deactivate();
    };

    const rollover = () => {
        over = true;
        eventHandler.handleEvent(ButtonEvents.ROLLOVER);
    };

    const rollout = () => {
        over = false;
        eventHandler.handleEvent(ButtonEvents.ROLLOUT);
    };

    const mousedown = () => {
        down = true;
        eventHandler.handleEvent(ButtonEvents.DOWN);
    };

    const mouseup = () => {
        down = false;
        eventHandler.handleEvent(ButtonEvents.UP);
    };

    mouseInteraction.click = click;
    mouseInteraction.rollover = rollover;
    mouseInteraction.rollout = rollout;
    mouseInteraction.mousedown = mousedown;
    mouseInteraction.mouseup = mouseup;

    const isActive = () => active;
    const isOver = () => over;
    const isDown = () => down;


    return Object.assign({
        activate,
        deactivate,
        click,
        simulateClick,
        isActive,
        isOver,
        isDown,
        disableInteraction,
    }, eventHandler)
}
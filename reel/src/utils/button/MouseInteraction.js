export const MouseInteraction = (clickLayer) => {

    let active = false;

    clickLayer.mouseover = () => {};
    clickLayer.mouseout = () => {};
    clickLayer.click = () => {};
    clickLayer.tap = () => {};
    clickLayer.mousedown = () => {};
    clickLayer.mouseup = () => {};
    clickLayer.mousemove = () => {};
    clickLayer.on = clickLayer.on || (() => {});

    const activate = () => {
        active = true;
        clickLayer.interactive = true;
        clickLayer.buttonMode = true;
    };

    const deactivate = () => {
        active = false;
        clickLayer.interactive = false;
        clickLayer.buttonMode = false;
    };

    const isActive = () => active;

    return {
        activate,
        deactivate,
        isActive,
        set click(value) {
            clickLayer.click = clickLayer.tap = value;
        },
        set rollover(value) {
            clickLayer.on('mouseover', value);
        },
        set rollout(value) {
            clickLayer.on('mouseout', value);
        },
        set mousedown(value) {
            clickLayer.on('mousedown', value);
            clickLayer.on('touchstart', value);
        },
        set mouseup(value) {
            clickLayer.on('mouseup', value);
            clickLayer.on('touchend', value);
            clickLayer.on('touchendoutside', value);
        },
        set mousemove(value) {
            clickLayer.on('pointermove', value);
        }
    }
}

import ButtonEvents from './ButtonEvents.js'
import {Button} from './Button.js'
import {EventHandler} from './EventHandler';

export const DisplayButton = ({clickLayer, animator, oneShot = true}) => {

    const button = Button({clickLayer, oneShot});
    const eventHandler = EventHandler(ButtonEvents);

    let over = false;
    let down = false;

    const activate = () => {
        if (button.isActive()) return void 0;
        button.activate();
        eventHandler.handleEvent(ButtonEvents.ACTIVATE);
        return animator.activate().then(() => {
            eventHandler.handleEvent(ButtonEvents.ACTIVATED);
        });
    };

    const deactivate = () => {
        if (!button.isActive()) return void 0;
        button.deactivate();
        eventHandler.handleEvent(ButtonEvents.DEACTIVATE);
        return animator.deactivate().then(() => {
            eventHandler.handleEvent(ButtonEvents.DEACTIVATED);
        });
    };

    const simulateClick = () => button.simulateClick();

    const click = () => {
        eventHandler.handleEvent(ButtonEvents.CLICK);
        if (oneShot) button.disableInteraction();
        return animator.click().then(() => {
            eventHandler.handleEvent(ButtonEvents.CLICKED);
            if (!button.isActive()) {
                animator.deactivate().then(() => {
                    eventHandler.handleEvent(ButtonEvents.DEACTIVATED);
                });
            }
        });
    };

    button.on(ButtonEvents.CLICK, click);

    button.on(ButtonEvents.ROLLOVER, () => {
        over = true;
        eventHandler.handleEvent(ButtonEvents.ROLLOVER);
        return animator.rollover().then(function () {
            eventHandler.handleEvent(ButtonEvents.ROLLOVER_COMPLETED);
        });
    });

    button.on(ButtonEvents.ROLLOUT, () => {
        over = false;
        eventHandler.handleEvent(ButtonEvents.ROLLOUT);
        return animator.rollout().then(function () {
            eventHandler.handleEvent(ButtonEvents.ROLLOUT_COMPLETED);
        });
    });

    button.on(ButtonEvents.DOWN, () => {
       down = true;
        eventHandler.handleEvent(ButtonEvents.DOWN);
    });

    button.on(ButtonEvents.UP, () => {
       down = false;
       eventHandler.handleEvent(ButtonEvents.UP);
    });

    const isActive = () => button.isActive();
    const isOver = () => button.isOver();
    const isDown = () => button.isDown();

    return Object.assign({
        activate,
        deactivate,
        simulateClick,
        isActive,
        isOver,
        isDown
    }, eventHandler)
}
import {DisplayButton} from "../utils/button/DisplayButton";
import {StandardAnimator} from "../utils/button/StandardAnimator";
import ButtonEvents from "../utils/button/ButtonEvents";
import gsap from "gsap/";
import {Reel} from "../components/reel";
import {delayedPromise, repeat} from "../utils/helpers";

export const GameScreen = DisplayList => {

    const root = DisplayList.getObject("root.gameScreen");
    const reelHolder  = root.getChildByName("reelHolder");
    const mask = root.getChildByName("maskImg");
    const btnDispGo = root.getChildByName("btnGo");
    const btnDispSpeed = root.getChildByName("btnSpeed");
    const btnDispMask = root.getChildByName("btnMask");


    let moving = false;
    let slowed = false;
    let masked = false;
    mask.visible = false;

    const reelLayout = [
        ["S","N","N","N","N","N","N","N","S","N","S","N","N","N","S"],
        ["N","S","N","N","N","S","N","N","N","N","S","N","N","S","N"],
        ["N","S","S","N","N","N","S","N","N","N","N","N","S","N","N"],
        ["N","N","N","S","N","N","N","S","N","N","N","S","N","N","S"],
        ["S","N","N","N","N","N","N","N","S","N","S","N","N","N","S"],
        ["S","S","N","N","N","N","N","N","N","N","N","N","N","S","S"],
    ];

    const urlParams = new URLSearchParams(window.location.search);
    const reelStartDelay = parseFloat(urlParams.get("startDelay") || "0.125");
    const reelStopDelay = parseFloat(urlParams.get("stopDelay") || "0.125");
    const reelStopTime = parseFloat(urlParams.get("stopTime") || "1.5");
    const reelStopEase = urlParams.get("stopEase") || "expo.out";


    const reels = repeat(5, i => Reel(reelHolder.getChildByName(`reel${i}`), DisplayList, {symbols:24, startY:-3264, reelSize:272, layout:reelLayout[i]}, i));

    const btn = DisplayButton({clickLayer:btnDispGo, animator:StandardAnimator(btnDispGo), oneShot:false});
    btn.on(ButtonEvents.CLICK, () => {
        console.log("START/STOP BUTTON CLICKED", Date.now());
        if(moving){
            moving = false;
            reels.forEach((r, i) => {
                delayedPromise(i * reelStopDelay).then(() => {
                    console.log(`REEL-${i}: STOPPING`, Date.now());
                    r.randomStop(reelStopTime, reelStopEase);
                })
            });
        }else {
            moving = true;
            reels.forEach((r, i) => {
                delayedPromise(i * reelStartDelay).then(() => {
                    console.log(`REEL-${i}: STARTING`, Date.now());
                    r.increaseSpeedTo(100)
                })
            });
        }
    });
    btn.activate();

    const btnSpeed = DisplayButton({clickLayer:btnDispSpeed, animator:StandardAnimator(btnDispSpeed), oneShot:false});
    btnSpeed.on(ButtonEvents.CLICK, () => {
        slowed = !slowed;
        gsap.ticker.fps(slowed ? 15 : 60)
    });
    btnSpeed.activate();



    const btnMask = DisplayButton({clickLayer:btnDispMask, animator:StandardAnimator(btnDispMask), oneShot:false});
    btnMask.on(ButtonEvents.CLICK, () => {
        masked = !masked;
        mask.visible = masked;
        reelHolder.mask = masked ? mask : null;
    });
    btnMask.activate();

    return {}
};
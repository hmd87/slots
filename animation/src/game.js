import {GameScreen} from "./screens/game";

export const Game = () => {


    const create = (displayList, PIXI) => GameScreen(displayList, PIXI);
    const start = () => document.getElementById("titleCard").style.display = "none";
    
    return {create, start};
};
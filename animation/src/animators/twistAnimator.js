import gsap from "gsap/";
import {DisplayButton} from "../utils/button/DisplayButton";
import {StandardAnimator} from "../utils/button/StandardAnimator";
import ButtonEvents from "../utils/button/ButtonEvents";

export const TwistAnimator = (display, PIXI) => {


    const btn = DisplayButton({clickLayer:display, animator:StandardAnimator(display)});
    const symbol0 = display.getChildByName("symbol0");
    const symbol1 = display.getChildByName("symbol1");
    symbol1.alpha = 0;

    const twistFilter = new PIXI.filters.TwistFilter({offset:{x:200, y:200}, radius:200, angle:0});
    display.filters = [ twistFilter ];

    let first = true;

    const tl0 = gsap.timeline({paused:true});
    tl0.to(twistFilter, {duration: 0.5, angle:20});
    tl0.to(symbol0, {alpha:0, duration: 0.2}, "-=0.3");
    tl0.to(symbol1, {alpha:1, duration: 0.2},"-=0.3");
    tl0.to(twistFilter, {duration: 0.5, angle:0, onComplete:btn.activate});


    const tl1 = gsap.timeline({paused:true});
    tl1.to(twistFilter, {duration: 0.5, angle:20});
    tl1.to(symbol1, {alpha:0, duration: 0.2}, "-=0.3");
    tl1.to(symbol0, {alpha:1, duration: 0.2},"-=0.3");
    tl1.to(twistFilter, {duration: 0.5, angle:0, onComplete:btn.activate});


    const animate = () => {
        first ? tl0.play(0) : tl1.play(0);
        first = !first;
    };

    btn.on(ButtonEvents.CLICK, animate);
    btn.activate();

    return { }
};

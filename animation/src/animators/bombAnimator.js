import gsap from "gsap/";
import {DisplayButton} from "../utils/button/DisplayButton";
import {StandardAnimator} from "../utils/button/StandardAnimator";
import ButtonEvents from "../utils/button/ButtonEvents";

export const BombAnimator = (display, PIXI) => {

    window.symbol0 = display;

    const btn = DisplayButton({clickLayer:display, animator:StandardAnimator(display)});

    let shouldRun = false;

    const shockwaveFilter = new PIXI.filters.ShockwaveFilter([160, 160]);
    display.filters = [ ];

    const Run = () => {
        if(!shouldRun) return;
        if(shockwaveFilter.time < 1){
            requestAnimationFrame(Run);
        }else{
            display.filters = [];
        }
        shockwaveFilter.time = (shockwaveFilter.time >= 1 ) ? 0 : shockwaveFilter.time + 0.01;
    };

    var tl = gsap.timeline({paused:true});
    tl.to(display, {x: "+=1", y:"-=1", duration: 0.05});
    tl.to(display, {x: "-=2", y:"+=2", duration: 0.1});
    tl.to(display, {x: "+=3", y:"-=3", duration: 0.1});
    tl.to(display, {x: "-=4",y:"+=4", duration: 0.1});
    tl.to(display, {x: "+=6",y:"-=6", duration: 0.1});
    tl.to(display, {x: "-=8",y:"+=8", duration: 0.1});
    tl.to(display, {x: "+=10",y:"-=10", duration: 0.1});
    tl.to(display, {x: "-=12",y:"+=12", duration: 0.1});
    tl.to(display, {x: "+=15",y:"-=15", duration: 0.1});
    tl.to(display, {x: "-=18",y:"+=18", duration: 0.1});
    tl.to(display, {x: "+=18", y:"-=18",duration: 0.1, onStart:() => {
            display.filters = [shockwaveFilter];
            shouldRun = true;
            Run();
    },
    });
    tl.to(display, {alpha:0, duration:0.25,delay:0.25});
    tl.to(display, { duration:0.5, onComplete:() => {
            shouldRun  = false;
            display.filters = [];
            shockwaveFilter.time = 0;
            display.x = 600;
            display.y = 10;
            display.alpha = 1;
            btn.activate();
        }});

    const animate = () => {
        tl.play(0);
    };

    btn.on(ButtonEvents.CLICK, animate);
    btn.activate();



    return { }
};

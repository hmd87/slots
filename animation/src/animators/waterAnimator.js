import {DisplayButton} from "../utils/button/DisplayButton";
import {StandardAnimator} from "../utils/button/StandardAnimator";
import ButtonEvents from "../utils/button/ButtonEvents";
import gsap from "gsap/";


export const WaterAnimator = (display, PIXI) => {

    const fragmentShader =
        `
        precision mediump float;
        varying vec2 vTextureCoord;
        uniform sampler2D uSampler;
        uniform float time;

        void main(void){
            vec4 pixel = texture2D(uSampler, vTextureCoord);
            pixel.rg = (time * pixel.rg);
            gl_FragColor = pixel;
        }`;

    const vertex  =
        `
        attribute vec2 aVertexPosition;
        attribute vec2 aTextureCoord;
        uniform mat3 projectionMatrix;
        varying vec2 vTextureCoord;
        void main(void){
            gl_Position = vec4((projectionMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);
            vTextureCoord = aTextureCoord;
        }`;

    const uniforms = {};
    uniforms.time = {type: '1f',value: 1};

    class MyFilter extends PIXI.Filter {
        constructor() {
            super(vertex, fragmentShader);
            this.time = 1;
        }
        apply(filterManager, input, output, clear) {
            /**
             * There is no set/get of `time`, for performance.
             * Because in the most real cases, `time` will be changed in ever game tick.
             * Use set/get will take more function-call.
             */
            this.uniforms.time = this.time;

            filterManager.applyFilter(this, input, output, clear);
        }


    }

    const bulge0 = [
        new PIXI.filters.BulgePinchFilter({radius:30, center:[Math.random(), 1.5], strength:2.5}),
        new PIXI.filters.BulgePinchFilter({radius:20, center:[Math.random(), 1.5], strength:2.5}),
        new PIXI.filters.BulgePinchFilter({radius:50, center:[Math.random(), 1.5], strength:1}),
        new PIXI.filters.BulgePinchFilter({radius:30, center:[Math.random(), 1.5], strength:2.35}),
        new PIXI.filters.BulgePinchFilter({radius:30, center:[Math.random(), 1.5], strength:1.5}),
        new PIXI.filters.BulgePinchFilter({radius:20, center:[Math.random(), 1.5], strength:1}),
        new PIXI.filters.BulgePinchFilter({radius:50, center:[Math.random(), 1.5], strength:1.25}),
        new PIXI.filters.BulgePinchFilter({radius:30, center:[Math.random(), 1.5], strength:2.5}),
        new PIXI.filters.BulgePinchFilter({radius:20, center:[Math.random(), 1.5], strength:2.5}),
        new PIXI.filters.BulgePinchFilter({radius:50, center:[Math.random(), 1.5], strength:1}),
        new PIXI.filters.BulgePinchFilter({radius:30, center:[Math.random(), 1.5], strength:2.35}),
        new PIXI.filters.BulgePinchFilter({radius:30, center:[Math.random(), 1.5], strength:1.5}),
        new PIXI.filters.BulgePinchFilter({radius:20, center:[Math.random(), 1.5], strength:1}),
        new PIXI.filters.BulgePinchFilter({radius:50, center:[Math.random(), 1.5], strength:1.25}),
        new PIXI.filters.BulgePinchFilter({radius:30, center:[Math.random(), 1.5], strength:2.5}),
        new PIXI.filters.BulgePinchFilter({radius:20, center:[Math.random(), 1.5], strength:2.5}),
        new PIXI.filters.BulgePinchFilter({radius:50, center:[Math.random(), 1.5], strength:1}),
        new PIXI.filters.BulgePinchFilter({radius:30, center:[Math.random(), 1.5], strength:2.35}),
        new PIXI.filters.BulgePinchFilter({radius:30, center:[Math.random(), 1.5], strength:1.5}),
        new PIXI.filters.BulgePinchFilter({radius:20, center:[Math.random(), 1.5], strength:1}),
        new PIXI.filters.BulgePinchFilter({radius:50, center:[Math.random(), 1.5], strength:1.25}),
        new PIXI.filters.BulgePinchFilter({radius:30, center:[Math.random(), 1.5], strength:2.5}),
        new PIXI.filters.BulgePinchFilter({radius:20, center:[Math.random(), 1.5], strength:2.5}),
        new PIXI.filters.BulgePinchFilter({radius:50, center:[Math.random(), 1.5], strength:1}),
        new PIXI.filters.BulgePinchFilter({radius:30, center:[Math.random(), 1.5], strength:2.35}),
        new PIXI.filters.BulgePinchFilter({radius:30, center:[Math.random(), 1.5], strength:1.5}),
        new PIXI.filters.BulgePinchFilter({radius:20, center:[Math.random(), 1.5], strength:1}),
        new PIXI.filters.BulgePinchFilter({radius:50, center:[Math.random(), 1.5], strength:1.25}),
    ];

    const myFilter = new MyFilter();
    display.filters = [myFilter, ...bulge0];

    const tl = gsap.timeline({paused:true});
    tl.to(myFilter, {time:0, duration:0.5, delay:0.25});
    bulge0.forEach((b, i) => {
        const p = {y:1.5};
        b.center[0] = (Math.random() * 0.8) + 0.1;
        tl.to(p, {duration:(Math.random() * 2) + 1, ease:"none", y:-0.5, delay:Math.random() / 1.5, onUpdate:() => {
                b.center[1] = p.y;
            },
        }, 0);
    });
    tl.to(myFilter, {
        time: 1, duration: 0.5, onComplete: () => {
            btn.activate();
        }
    });
    
    const btn = DisplayButton({clickLayer:display, animator:StandardAnimator(display)});
    const animate = () => {
        tl.play(0);
    };

    btn.on(ButtonEvents.CLICK, animate);
    btn.activate();
};
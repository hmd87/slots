import gsap from "gsap/";
import {DisplayButton} from "../utils/button/DisplayButton";
import {StandardAnimator} from "../utils/button/StandardAnimator";
import ButtonEvents from "../utils/button/ButtonEvents";

export const GlowAnimator = (display, PIXI) => {
    
    const btn = DisplayButton({clickLayer:display, animator:StandardAnimator(display)});
    const symbol0 = display.getChildByName("symbol0");
    const symbol1 = display.getChildByName("symbol1");
    
    symbol1.alpha = 0;
    symbol1.blendMode = PIXI.BLEND_MODES.ADD;
    
    const tl = gsap.timeline({paused:true, repeat:-1});
    tl.to(display, {duration:0.05, rotation:"+=0.0872665"});
    tl.to(display, {duration:0.1, rotation:"-=0.174533"});
    tl.to(display, {duration:0.05, rotation:"+=0.0872665"});

    const tl1 = gsap.timeline({paused:true, repeat:-1});
    tl1.to(symbol1, {alpha:1, duration:0.1});
    tl1.to(symbol1, {alpha:0, duration:0.1});

    const tl2 = gsap.timeline({paused:true});
    tl2.to(symbol0, {rotation:0.436332, duration:0, delay:0.6});
    tl2.to(symbol1, {rotation:0.436332, duration:0});
    tl2.to(display.scale, {x:1.1, y:1.1, duration:0});
    tl2.to(symbol0, {rotation:-0.610865, duration:0, delay:0.6});
    tl2.to(symbol1, {rotation:-0.610865, duration:0});
    tl2.to(display.scale, {x:1.2, y:1.2, duration:0});
    tl2.to(display.scale, {x:1.5, y:1.5, duration:0, delay:0.6, onStart:() => {
            display.rotation = 0;
            symbol0.rotation = 0;
            symbol1.rotation = 0;
            tl.pause();
            tl1.pause();
        }
    });
    tl2.to(symbol1, {duration:0.25, alpha:1});
    tl2.to(symbol1.scale, {duration:0.5, x:2, y:2}, "-=0.25");
    tl2.to(symbol0.scale, {duration:0.5, x:0, y:0}, "-=0.5");
    tl2.to(symbol0, {duration:0.25, alpha:0}, "-=0.25");
    tl2.to(symbol1, {duration:0.25, alpha:0, onComplete:() => {
            symbol0.scale.set(1, 1);
            symbol1.scale.set(1, 1);
            display.scale.set(1, 1);
            symbol0.alpha = 1;

            btn.activate();
        }
    }, "-=0.25");
    
    const animate = () => {
        tl.play(0);
        tl1.play(0);
        tl2.play(0);
    };

    btn.on(ButtonEvents.CLICK, animate);
    btn.activate();



    return { }
};

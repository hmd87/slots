import gsap from "gsap/";

export const FadeAnimator = display => {

    const animateAlpha = (alpha, duration) => new Promise(onComplete => gsap.to(display, {alpha, duration,  onComplete}));

    const rollover = () => animateAlpha(0.25, 0);
    const rollout = () => animateAlpha(1, 0);
    const click = () => animateAlpha(1, 0);
    const activate = () => animateAlpha(1 , 0.25);
    const deactivate = () => animateAlpha(0 , 0.25);

    return {rollover, rollout, click, activate, deactivate}

}

import {BombAnimator} from "../animators/bombAnimator";
import {WaterAnimator} from "../animators/waterAnimator";
import {TwistAnimator} from "../animators/twistAnimator";
import {GlowAnimator} from "../animators/glowAnimator";

export const GameScreen = (DisplayList, PIXI) => {

    const root = DisplayList.getObject("root.gameScreen");
    BombAnimator(root.getChildByName("symbol0"), PIXI);
    TwistAnimator(root.getChildByName("symbol1"), PIXI);
    GlowAnimator(root.getChildByName("symbol2"), PIXI);
    WaterAnimator(root.getChildByName("symbol3"), PIXI);
    
    return {}
};
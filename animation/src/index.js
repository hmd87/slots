import {Bootstrap} from "../../pixi-bootstrap/build/index.js";
import {Game} from "./game";
import Config from "./config.json";

const bootstrap = Bootstrap({height:Config.size.height, width:Config.size.width, backgroundColor:0xCCCCCC});
const game = Game();

bootstrap.setup().then(displayList => {
    return game.create(displayList, bootstrap.PIXI);
}).then(game.start);
const CLICK                     =   "click";
const CLICKED                   =   "clicked";
const ACTIVATE                  =   "activate";
const ACTIVATED                 =   "activated";
const DEACTIVATE                =   "deactivate";
const DEACTIVATED               =   "deactivated";
const ROLLOVER                  =   "rollover";
const ROLLOUT                   =   "rollout";
const ROLLOUT_COMPLETED         =   "rolloutCompleted";
const ROLLOVER_COMPLETED        =   "rolloverCompleted";
const DOWN                      =   "mousedown";
const UP                        =   "mouseup";

export default {CLICK, CLICKED, ACTIVATE, ACTIVATED, DEACTIVATE, DEACTIVATED, ROLLOVER, ROLLOUT, ROLLOUT_COMPLETED, ROLLOVER_COMPLETED, DOWN, UP}
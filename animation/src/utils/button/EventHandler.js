export const EventHandler = eventList => {

    let events = {};
    let eventsOnce = {};
    const isValidEvent = event => events[event] !== undefined;
    const warnInvalidEvent = event => console.warn(`Event type: ${event} not supported for this object`);
    const callEvents = events => events.forEach(Function.prototype.call, Function.prototype.call);

    const addEvent = (type, event, callback, priority) => {
        if (!isValidEvent(event)) {
            warnInvalidEvent(event);
            return void 0;
        }
        if (priority === -1) {
            type[event].push(callback);
        } else {
            type[event].splice(priority, 0, callback);
        }
    };

    const once = function(event, callback, priority = -1) {
        addEvent(eventsOnce, ...arguments);
    };

    const on = function(event, callback, priority = -1) {
        addEvent(events, ...arguments);
    }

    const off = (event, callback) => {
        if (!isValidEvent(event)) {
            warnInvalidEvent(event);
            return void 0;
        }
        const index = events[event].indexOf(callback);
        if (index === -1) {

        } else {
            events[event].splice(index, 1);
        }
    };

    const handleEvent = eventType => {
        callEvents(eventsOnce[eventType]);
        callEvents(events[eventType]);
        eventsOnce[eventType] = [];
    };

    const clearAllEvents = eventType => {
        if (eventType) {
            events[eventType] = [];
            eventsOnce[eventType] = [];
        } else {
            events = {};
            eventsOnce = {};
            Object.keys(eventList).map(key => {
                events[eventList[key]] = [];
                eventsOnce[eventList[key]] = [];
            });
        }
    };

    clearAllEvents();

    return {
        once,
        on,
        off,
        handleEvent,
        clearAllEvents,
    }
}
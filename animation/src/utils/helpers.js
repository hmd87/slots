import gsap from "gsap/";

export const invoke = (arr, func) => arr.forEach(x => x[func]());
export const partial = (func, ...args) => () => func(...args);
export const randomElement = arr => arr[Math.floor(Math.random() * arr.length)];
export  const delayedPromise = delay => {
    return new Promise(resolve => {
        gsap.delayedCall(delay, resolve);
    })
};
export const randomNumber = (min, max) => (Math.random() * max) + min;

export const repeat = (time, func) => {
    const results = [];
    for(let i = 0; i < time; i++) results.push(func(i));
    return results;
};

export const sanitise = (arr, max) => {
    return arr.map(x => x >= max ? x - max : x);
};
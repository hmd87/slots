import {DisplayButton} from "./button/DisplayButton";
import {FadeAnimator} from "../animators/fadeAnimator";
import {AudioPlayer} from "../../../pixi-bootstrap/build/index.js";
import ButtonEvents from "./button/ButtonEvents";


export const SoundButton = display => {

    const button = DisplayButton({clickLayer:display, animator:FadeAnimator(display), oneShot:false});
    const onDisp = display.getChildByName("on");
    const offDisp = display.getChildByName("off");

    button.on(ButtonEvents.CLICK, () => {
        const soundOff = AudioPlayer.toggleMute();
        offDisp.visible = soundOff;
        onDisp.visible = !soundOff;
    });


    button.activate();


}